import { useEffect, useState } from "react";
import { useNavigate } from 'react-router-dom';

function AutomobileList() {
    const [autos, setAutos] = useState([]);
    const navigate = useNavigate();

    const getData = async () => {
        try {
            const response = await fetch('http://localhost:8100/api/automobiles/');
            if (response.ok) {
                const data = await response.json();
                setAutos(data.autos);
            } else {
                console.error('Failed to fetch Automobiles:', response.statusText);
            }
        } catch (error) {
            console.error('Error fetching automobiles:', error);
        }
    };

    useEffect(() => {
        getData();
    }, []);

    const deleteAutos = async (autoVin) => {
        try {
            const response = await fetch(`http://localhost:8100/api/automobiles/${autoVin}`, {
                method: "DELETE",
            });

            if (response.ok) {
                getData();
            } else {
                const errorData = await response.json();
                console.error('failed to delete automobile:', errorData.message);
            }
        } catch (error) {
            console.error('error deleting automobile:', error);
        }
    };

    const navToNewAuto = () => {
        navigate('/automobiles/new'); 
    };

    return (
        <div>    
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>VIN</th>
                        <th>Color</th>
                        <th>Year</th>
                        <th>Model</th>
                        <th>Manufacturer</th>
                        <th>Sold</th>
                    </tr>
                </thead>
                <tbody>
                    {autos.map(auto => (
                        <tr key={auto.id}>
                            <td>{auto.vin}</td>
                            <td>{auto.color}</td>
                            <td>{auto.year}</td>
                            <td>{auto.model.name}</td>
                            <td>{auto.model.manufacturer.name}</td>
                            <td>{auto.sold ? "Yes" : "No"}</td>
                            <td>
                                <button 
                                    className="btn btn-danger" 
                                    onClick={() => deleteAutos(auto.vin)}
                                >
                                    Delete
                                </button>
                            </td>
                        </tr>
                    ))}
                </tbody>
            </table>
            <button type="button" className="btn btn-primary" onClick={navToNewAuto}>
                Add Automobile
            </button>
        </div>
    );
}

export default AutomobileList;
