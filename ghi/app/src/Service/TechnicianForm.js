import React, { useState, useEffect} from 'react';
import { useNavigate } from 'react-router-dom';

function TechnicianForm() {
    const [technicians, setTechnicians] = useState([]);

    const getTechnicians = async () => {
    const response = await fetch("http://localhost:8080/api/technicians/")
    
        if (response.ok) {
            const data = await response.json();
            setTechnicians(data.technician);
        }
    };

    useEffect(() => {
        getTechnicians();
    }, [])


    const [technician, setTechnician] = useState({
        first_name: '',
        last_name: '',
        employee_id: ''
    });

    const handleChange = (event) => {
        setTechnician({ ...technician, [event.target.name]: event.target.value });
    }


    const [submitStatus, setSubmitStatus] = useState("");
    const navigate = useNavigate();

    const handleSubmit = async (event) => {
        event.preventDefault();

        const locationUrl = 'http://localhost:8080/api/technicians/'; // Adjusted URL
        const fetchConfig = {
            method: 'POST', // POST method for creating a new entry
            body: JSON.stringify(technician),
            headers: {
                'Content-Type': 'application/json',
            },
        };

        try {
            const response = await fetch(locationUrl, fetchConfig);
            if (response.ok) {
                setTechnician({ 
                    first_name: '',
                    last_name: '',
                    employee_id: ''
                });

                setSubmitStatus('Technician added successfully');
                setTimeout(() => setSubmitStatus(''), 2000);

            } else {
                const errorData = await response.json();
                setSubmitStatus("error: " + errorData.message)

            }
        } catch (error) {
            console.error('Error submitting form', error);
            setSubmitStatus('Submission failed: ' + error.message)
        }
    };
    const navToTechnicians = () => {
        navigate('/technicians/'); 
};

  return (
    <div className="row">
        <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
            <h1>Add a Technician</h1>

                <form onSubmit={handleSubmit} id="create-technician-form">

                    <div className="form-floating mb-3">
                        <input value={technician.first_name} onChange={handleChange} placeholder="First name..." required type="text" name="first_name" id="first_name" className="form-control" />
                        <label htmlFor='first_name'>First name...</label>
                    </div>

                    <div className="form-floating mb-3">
                        <input value={technician.last_name} onChange={handleChange} placeholder="Last name..." required type="text" name="last_name" id="last_name" className="form-control" />
                        <label htmlFor='last_name'>Last name...</label>
                    </div>

                    <div className="form-floating mb-3">
                        <input value={technician.employee_id} onChange={handleChange} placeholder="Employee ID..." required type="text" name="employee_id" id="employee_id" className="form-control" />
                        <label htmlFor='employee_id'>Employee ID...</label>
                    </div>

                    <button type="submit" className="btn btn-primary me-3">Create</button>
                    <button type="button" className="btn btn-secondary" onClick={navToTechnicians}>Technicians</button>

                    {submitStatus && <div className='alert alert-info mt-3'>{submitStatus}</div>}
                </form>
            </div>
        </div>
    </div>
    );
}

export default TechnicianForm;
