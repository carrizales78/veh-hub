import { useEffect, useState } from "react";
import { useNavigate } from 'react-router-dom';

function ServiceHistory() {
    const [appointments, setAppoinments] = useState([]);
    const navigate = useNavigate();

    const getData = async () => {
        try {
            const response = await fetch(`http://localhost:8080/api/appointments/`);
            if (response.ok) {
                const data = await response.json();
                setAppoinments(data.appointments);
            } else {
                console.error('Failed to fetch status:', response.statusText);
            }
        } catch (error) {
            console.error('Error fetching status:', error);
        }
    };

    useEffect(() => {
        getData();
    }, []);

    const deleteAppt = async (apptId) => {
        try {
            const response = await fetch(`http://localhost:8080/api/appointments/${apptId}`, {
                method: "DELETE",
            });

            if (response.ok) {
                getData();
            } else {
                const errorData = await response.json();
                console.error('failed to delete appointment:', errorData.message);
            }
        } catch (error) {
            console.error('error deleting appointment:', error);
        }
    };

    const navToAppointments = () => {
        navigate('/appointments/'); 
    };

    return (
        <div>    
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>VIN</th>
                        <th>VIP</th>
                        <th>Customer</th>
                        <th>Date</th>
                        <th>Time</th>
                        <th>Technician</th>
                        <th>Reason</th>
                        <th>Status</th>
                    </tr>
                </thead>
                <tbody>
                    {appointments.map(appointment => {
                        const dateTime = new Date(appointment.date_time)
                        const date = dateTime.toLocaleDateString("en-US")
                        const time = dateTime.toLocaleTimeString("en-US")  
                        const techName = `${appointment.technician.first_name} ${appointment.technician.last_name}`

                        return (
                        <tr key={appointment.id}>
                            <td>{appointment.vin}</td>
                            <td>{appointment.is_vip ? "Yes" : "No"}</td>
                            <td>{appointment.customer}</td>
                            <td>{date}</td>
                            <td>{time}</td>
                            <td>{techName}</td>
                            <td>{appointment.reason}</td>
                            <td>{appointment.status}</td>
                            <td>
                                <button 
                                    className="btn btn-danger" 
                                    onClick={() => deleteAppt(appointment.id)}
                                >
                                Delete
                                </button>
                            </td>
                        </tr>
                        )
                    })}
                </tbody>
            </table>
            <button type="button" className="btn btn-primary" onClick={navToAppointments}>
                Appointments
            </button>
        </div>
    );
}

export default ServiceHistory;
