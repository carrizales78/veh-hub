from django.db import models
from django.urls import reverse


class Technician(models.Model):

    first_name = models.CharField(max_length=100)
    last_name = models.CharField(max_length=100)
    employee_id =models.PositiveIntegerField(unique=True, null=True)

   
    def get_api_url(self):
        return reverse("detail_technician", kwargs={"id": self.pk})
    

class AutomobileVO(models.Model):
    import_href = models.CharField(max_length=200, unique=True)
    vin = models.CharField(max_length=17, unique=True)
    sold = models.BooleanField(default=False)

    def get_api_url(self):
        return reverse("api_automobile", kwargs={"vin": self.vin})


class Appointment(models.Model):

    date_time = models.DateTimeField()
    reason = models.CharField(max_length=200)
    status = models.CharField(max_length=50, default='scheduled')
    vin = models.CharField(max_length=17)
    customer = models.CharField(max_length=100)
    
    technician = models.ForeignKey(
        Technician,
        related_name="appointments",
        on_delete=models.CASCADE
    ) 
    
    def get_api_url(self):
        return reverse("detail_appointment", kwargs={"id": self.pk})
    
    class Meta:
        ordering = ("customer", "vin")
