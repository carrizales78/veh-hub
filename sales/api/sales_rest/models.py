from django.db import models
from django.urls import reverse


class Salesperson(models.Model):
    first_name = models.CharField(max_length=100)
    last_name = models.CharField(max_length=100)
    employee_id = models.PositiveSmallIntegerField(unique=True, null=True)

    def get_api_url(self):
        return reverse("detail_salesperson", kwargs={"id": self.pk})


class Customer(models.Models):
    first_name = models.CharField(max_length=100)
    last_name = models.CharField(max_length=100)
    address = models.TextField(blank=True, null=True)
    phone_number = models.CharField(max_length=15)

    def get_api_url(self):
        return reverse("detail_customer", kwargs={"id": self.pk})


class Sale(models.Model):
    price = models.DecimalField(max_digits=10, decimal_places=2)

    automobile = models.ForeignKey(
        'Automobile',
        related_name="automobile",
        on_delete=models.CASCADE
    )

    salesperson = models.ForeignKey(
        'Salesperson',
        related_name="salesperson",
        on_delete=models.CASCADE
    )

    customer = models.ForeignKey(
        'Customer',
        related_name="customer",
        on_delete=models.CASCADE
    )

    def get_api_url(self):
        return reverse("detail_sale", kwargs={"id": self.pk})

    class Meta:
        ordering = ("customer", "vin")


class AutomobileVO(models.Model):
    vin = models.CharField(max_length=17, unique=True)
    sold = models.BooleanField(default=False)

    def get_api_url(self):
        return reverse("api_automobile", kwargs={"vin": self.vin})
